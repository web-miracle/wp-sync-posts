<?php 
include_once plugin_dir_path( __FILE__ ) . '../inc/Option.php';
use Miracle\inc\Option as Option;
?>
<h2>
	<?php echo get_admin_page_title() ?>
</h2>
<form id="import-form">
	<label for="tsv-link">
		Загрузите ссылку для импорта:
	</label>
	<input id="tsv-link" type="url" placeholder="link" required pattern="(https:\/\/docs.google.com\/)(.+)(tsv)" title="Обязательно расширение TSV">
	<br>
	<?php submit_button('Загрузить'); ?>
</form>
<p>
Всего постов в таблице: <span id="all">0</span>
</p>
<p>
В ожидании загрузки: <span id="waiting">0</span>
</p>
<p>
Загружено успешно: <span id="loaded">0</span>
</p>
<p>
Количество ошибок при выполнении: <span id="errored">0</span>
</p>
<table id="output"></table>
<script>
var form = document.getElementById('import-form');
var loaded = 0;
var errored = 0;
var all = 0;
var waiting = 0;
form.addEventListener('submit', function(event) {
  event.preventDefault();

  var linkToCsv = document.getElementById('tsv-link').value;
  var data = {
      action: "<?= Option::getAjax()->get_tsv ?>",
      link: linkToCsv
    };
  
  document.getElementById('tsv-link').value = '';

  alert('Ожидайте идет загрузка данных...');
  jQuery.post( ajaxurl, data, function(response) {
    response = JSON.parse(response);
    all = response.length;
    waiting = response.length;
    loaded = 0;
    errored = 0;
    updateData();
    document.getElementById('output').innerHTML = '';
    for (var i = 0; i < response.length; i++) {
      var column = document.createElement('tr');

      for (var j = 0; j < response[i].length; j++) {
        var row = document.createElement('td');
        row.style.border = '1px solid';
        row.innerText = response[i][j];
        column.appendChild(row);
      }
      document.getElementById('output').appendChild(column);
    }
    
    var titles = response[0];
    requestAnimationFrame(function() {
      for (let i = 0; i < response.length; i++) {
        var j = i;
        var insertData = {
          action: "<?= Option::getAjax()->import ?>",
          title: titles,
          post: response[i]
        }
        var lastRow = i + 1 === response.length;
        ajaxToInsert(ajaxurl, insertData, i, 1, lastRow);
      }
    })
  })
  .fail(function() {
    alert('Таблица не получена');
  });
});
  
function ajaxToInsert(ajaxurl, insertData, i, j, lastRow) {
  jQuery.post(
    ajaxurl,
    insertData,
    function(postResponse) {
      document.getElementById('output').querySelector('tr:nth-of-type(' + (i + 1) +')').style.backgroundColor = "#8cec8c";
      loaded++;
      waiting--;
      updateData();
    }
  ).fail(function() {
    errored++;
    updateData();
    console.log("Строка " + (i + 1) + " не вставлена" );
    document.getElementById('output').querySelector('tr:nth-of-type(' + (i + 1) +')').style.backgroundColor = "#ea6b6b";
    if (j < 50) {
      ajaxToInsert(ajaxurl, insertData, i, j+1, lastRow);
    }
  }).always(function() {
    if (lastRow) {
      alert("Работа скрипта окончена. Но скрипт еще продолжить работать");
    }
  });
}

function updateData(){
  document.getElementById('loaded').innerHTML = loaded;
  document.getElementById('waiting').innerHTML = waiting;
  document.getElementById('errored').innerHTML = errored;
  document.getElementById('all').innerHTML = all;
}
</script>