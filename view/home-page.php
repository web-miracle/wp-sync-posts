<?php 
    include_once plugin_dir_path( __FILE__ ) . '../inc/Option.php';
    use Miracle\inc\Option as Option;
?>
<div class="container">
    <div id="app">
        <div class="wrap">
            <div @click="openOperation('import')" class="item">
                Импорт постов
            </div>
            <div @click="openOperation('export')" class="item">
                Экспорт постов
            </div>
        </div>
        <div class="field">
            <?php
                submit_button('Сгенерировать таблицу', 
                    'primary', 
                    '', 
                    false, 
                    array(
                        '@click' => 'giveMeData()',
                        'ref'    => 'generate_button',
                        'data-action' => Option::getAjax()->export
                    )
                );
            ?>
            <table v-if="!loading">
                <tbody>
                    <tr v-for="(row, index) in exportData" :key="index">
                        <td v-for="(element, index2) in row" :key="index2">
                            {{ element }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="loader" v-if="loading">loading...</div>
        </div>
    </div>
</div>