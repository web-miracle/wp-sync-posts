<?php 
include_once plugin_dir_path( __FILE__ ) . '../inc/Option.php';
use Miracle\inc\Option as Option;
?>
<h2>
	<?php echo get_admin_page_title() ?>
</h2>
<?php submit_button('Сгенерировать таблицу'); ?>
<table id="output"></table>
<script>
var button = document.getElementById('submit');
button.addEventListener('click', function(event) {
	event.preventDefault();

	var data = {
			action: "<?= Option::getAjax()->export ?>"
		};

	alert('Ожидайте идет загрузка данных...');
	jQuery.post( ajaxurl, data, function(response) {
		response = JSON.parse(response);
		alert('Посты сгенерированы');

		for (var i = 0; i < response.length; i++) {
			var column = document.createElement('tr');

			for (var j = 0; j < response[i].length; j++) {
				var row = document.createElement('td');
				row.style.border = '1px solid';
				row.innerText = response[i][j];
				column.appendChild(row);
			}
			document.getElementById('output').appendChild(column);
		}
	});
});
</script>