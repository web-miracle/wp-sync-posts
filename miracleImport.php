<?php

/**
 * Plugin Name: WP Sync Posts
 * Description: Плагин преднозначен для обновления и импорта постов через переданную ссылку на google таблицу в формате tsv
 * Version: 0.2
 * Author: Web Miracle
 * Author URI: https://vk.com/web_miracle
 */

if ( !defined('WPINC') ) {
	die;
}

/**
 * Composer autoloader
 */
include plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

use Miracle\inc\Option as Option;

define('MIRACLE_PLUGIN', plugin_dir_path( __FILE__ ));

add_action( 'admin_menu', 'register_menu_item' );
add_action( 'admin_enqueue_scripts', 'include_js' );


$optionPage = array();
$optionPage['page_title'] = "WP Sync Posts";
$optionPage['menu_title'] = "Import";
$optionPage['capability'] = "manage_options";
$optionPage['menu_slug']  = "miracle_import_option_page";
$optionPage['view_file']  = "view/import-page.php";

$option = new Option( $optionPage );
$option->init();

$optionPage2 = array();
$optionPage2['page_title'] = "WP Sync Posts";
$optionPage2['menu_title'] = "Export";
$optionPage2['capability'] = "manage_options";
$optionPage2['menu_slug']  = "miracle_export";
$optionPage2['view_file']  = "view/export-page.php";

$option2 = new Option( $optionPage2 );
$option2->init();
 
function register_menu_item() {
	add_menu_page(
		"WP Sync Posts",
		"WP Sync Posts",
		"manage_options",
		"/miracleImport/view/home-page.php",
		null,
		"dashicons-cloud",
		"71"
	);
}

function include_js () {
	wp_enqueue_script('vuejs', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js');
	wp_enqueue_script('wp-sync-vuejs', plugin_dir_url( __FILE__ ) . '/assets/js/index.js', array('vuejs'));
	wp_enqueue_style('wp-sync-styles', plugin_dir_url( __FILE__ ) . '/assets/css/style.css');
}
