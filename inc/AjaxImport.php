<?php

namespace Miracle\inc;

use Miracle\inc\Ajax as Ajax;
use Miracle\inc\IAjax as IAjax;

/**
 * Класс обработчик ajax запроса импорта tsv данных
 */
class AjaxImport extends Ajax implements IAjax
{
	private static $title;

	public function callback() {
		
		AjaxImport::$title = $_POST['title'];
		echo AjaxImport::insertPost($_POST['post']);

		wp_die();
	}

	public static function insertPost($post) {
		foreach (AjaxImport::$title as $key => $value):
			if ($value === 'post_category'):
				$my_post['post_category'] = explode(', ', $post[$key]);
				foreach ($my_post['post_category'] as $key => $value) {
					$my_post['post_category'][$key] = intval($my_post['post_category'][$key]);
				}
			elseif ($value === 'ID' && !intval($post[$key])):
				$post[$key] = 0;
			endif;
		
			preg_match('/(cat_)(.*)/', $value, $matches, PREG_OFFSET_CAPTURE);
		
			if ($matches[1][0] === 'cat_') {
				$value = $matches[2][0];
				$my_post['tax_input'][$value] = explode(', ', $post[$key]);
			}

			$my_post['comment_status'] = "open";
			$my_post[$value] = $post[$key];
		endforeach;

		if ( $post_id = wp_insert_post($my_post) ):

		else:
			unset($my_post['ID']);
			$post_id = wp_insert_post($my_post);
		endif;

		//Set title
		update_post_meta( $post_id, '_yoast_wpseo_title', $my_post['seo title'] );
		//Set description
		update_post_meta( $post_id, '_yoast_wpseo_metadesc', $my_post['meta description'] );
		//Set keywords
		update_post_meta( $post_id, '_yoast_wpseo_focuskw', $my_post['keywords'] );
		return $post_id;
	}
}