<?php

namespace Miracle\inc;

use Miracle\inc\AjaxImport as AjaxImport;
use Miracle\inc\AjaxExport as AjaxExport;
use Miracle\inc\AjaxGetTsv as AjaxGetTsv;

/**
 * 
 */
class Option
{
	/**
	 * Заголовок страницы настроек
	 * @var string
	 */
	private $page_title;

	/**
	 * Текст, который будет использован в качестве называния для пункта меню.
	 * @var string
	 */
	private $menu_title;

	/**
	 * Название права доступа для пользователя, чтобы ему был показан этот пункт меню.
	 * @var string один из возможных значений
	 *             (Capability, Super Admin, Administrator, Editor, Author,	Contributor, Subscriber)
	 */
	private $capability;

	/**
	 * Идентификатор меню. Нужно вписывать уникальную строку, пробелы не допускаются.
	 * @var string
	 */
	private $menu_slug;

	/**
	 * Путь от папки плагина до файла, который будет выводить страницу настроек плагина
	 * @var string
	 */
	private $view_file;

	/**
	 * Объект в котором хранятся данные ajax
	 * @var object
	 */
	private $ajax;
	
	function __construct($args)
	{
		$default = array(
				"page_title" 		=> "Заголовок страницы",
				"menu_title"		=> "Пункт меню",
				"capability"		=> "manage_options",
				"menu_slug"			=> "miracle_import_option_page",
			);

		$args = wp_parse_args( $args, $default );

		$this->page_title = $args['page_title'];
		$this->menu_title = $args['menu_title'];
		$this->capability = $args['capability'];
		$this->menu_slug  = $args['menu_slug'];
		$this->view_file  = $args['view_file'];
		
		$this->ajax = (object)array( "import" => "tsv-import-ajax", "export" => "tsv-receive-ajax", "get_tsv" => "get-tsv" );
	}

	public function init() {
		add_action( 'admin_menu', array( $this, 'registerPage' ) );
		
		$importAjax = new AjaxImport($this->ajax->import);
		$importAjax->run();
		
		$exportAjax = new AjaxExport($this->ajax->export);
		$exportAjax->run();
		
		$getTSVAjax = new AjaxGetTsv($this->ajax->get_tsv);
		$getTSVAjax->run();
	}

	public function registerPage() {
		add_options_page(
			$this->page_title,
			$this->menu_title,
			$this->capability,
			$this->menu_slug,
			array( $this, 'view' )
		);
	}

	public function view() {
		include MIRACLE_PLUGIN . $this->view_file;
	}

	public static function getAjax() {
		return (object)array( "import" => "tsv-import-ajax", "export" => "tsv-receive-ajax", "get_tsv" => "get-tsv" );
	}
}