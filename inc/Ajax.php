<?php

namespace Miracle\inc;

/**
 * 
 */
abstract class Ajax
{
	/**
	 * Слуг, на который вешается обработчик запроса
	 * @var string
	 */
	private $action;
	
	function __construct($action)
	{
		$this->action = $action;
	}

	/**
	 * Функция привязки функции обратного вызова к запросу
	 * @return void
	 */
	public function run() {
		add_action('wp_ajax_' . $this->action, array($this, 'callback'));
	}
}

interface IAjax {

	/**
	 * Функция обратного вызова
	 * @return any
	 */
	public function callback();
}