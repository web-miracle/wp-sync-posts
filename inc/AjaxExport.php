<?php

namespace Miracle\inc;

use Miracle\inc\Ajax as Ajax;
use Miracle\inc\IAjax as IAjax;

/**
 * Класс обработчик ajax запроса импорта tsv данных
 */
class AjaxExport extends Ajax implements IAjax
{

	public function callback() {
		$result = array();

		$title = array(
			'ID',
			'post_type',
			'post_title',
			'post_name',
			'post_content',
			'post_category',
			'tags_input',
			'seo title',
			'meta description',
			'keywords',
			'post_status'
		);
		
		$post_tax = get_taxonomies();

		foreach ($post_tax as $tax) {
			if ($tax !== 'category' && $tax !== 'post_tag' && $tax !== 'nav_menu' && $tax !== 'link_category' && $tax !== 'post_format' ) {
				$title[] = 'cat_' . $tax;
			}
		}

		array_push($result, $title);

		foreach (AjaxExport::getPosts() as $post) {
			$categories = get_the_category($post->ID);
			$post_cat = array();
			foreach ($categories as $category) {
				array_push($post_cat, $category->cat_ID);
			}

			$tags = get_the_tags($post->ID);
			$post_tags = array();
			foreach ($tags as $tag) {
				array_push($post_tags, $tag->name);
			}
			
			$post_term = array();
			foreach ($post_tax as $tax) {
				if ($tax !== 'category' && $tax !== 'post_tag' && $tax !== 'nav_menu' && $tax !== 'link_category' && $tax !== 'post_format' ) {
					$terms = get_the_terms( $post->ID, $tax );
					foreach ($terms as $term) {
						$post_term[$tax][] = $term->name;
					}
				}
			}

			$row = array(
				$post->ID,
				$post->post_type,
				$post->post_title,
				$post->post_name,
				$post->post_content,
				$post_cat,
				$post_tags,
				get_post_meta($post->ID, '_yoast_wpseo_title')[0],
				get_post_meta($post->ID, '_yoast_wpseo_metadesc')[0],
				get_post_meta($post->ID, '_yoast_wpseo_focuskw')[0],
				$post->post_status
			);
			
			foreach ($post_tax as $tax) {
				if ($tax !== 'category' && $tax !== 'post_tag' && $tax !== 'nav_menu' && $tax !== 'link_category' && $tax !== 'post_format' ) {
					$row[] = $post_term[$tax];
				}
			}

			array_push($result, $row);
		}

		echo json_encode($result);
		wp_die();
	}

	public static function getPosts() {
		$args = array(
			'numberposts' => -1,
			'orderby'     => 'ID',
			'order'       => 'DESC',
			'post_type'   => array('post','page'),
			'post_status' => 'any'
		);

		return get_posts($args);
	}
}