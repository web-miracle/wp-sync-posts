<?php

namespace Miracle\inc;

use Miracle\inc\Ajax as Ajax;
use Miracle\inc\IAjax as IAjax;

/**
 * Класс обработчик ajax запроса получения всего tsv документа
 */
class AjaxGetTsv extends Ajax implements IAjax
{
	public function callback() {
		$result = array();

		// $file = plugin_dir_path( __FILE__ ) . "result.tsv";
		// $content = file_get_contents($_POST['link']);
		
		if (($tsv_table = fopen($_POST['link'], "r")) !== FALSE){
			while (($row = fgetcsv($tsv_table, 0, "	")) !== FALSE):
				array_push($result, $row);
			endwhile;
			// try{
			// 	json_encode($result);
			// 	wp_die();
			// }
			// catch(Exception $e){
			// 	echo $e->getMessage();
			// 	wp_die();
			// }
			
			fclose($tsv_table);
			// unlink($file);
		}
		
		echo json_encode($result);

		wp_die();
	}
}