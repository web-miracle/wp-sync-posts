document.addEventListener('DOMContentLoaded', () => {
    var app = new Vue({
        el: document.querySelector('#app'),
        data: {
          loading: true,
          exportData: []
        },
        mounted () {
            this.loading = false;
        },
        computed: {
            exportBody () {
                return []
            }
        },
        methods: {
            giveMeData () {
                let fd = new FormData();
                fd.append('action', this.$refs.generate_button.dataset.action)
                this.loading = true;
                let promise = fetch(ajaxurl, {
                    method: 'POST',
                    body: fd
                })
                    .then((response) => {
                        this.loading = false
                        return response.json()
                    })
                    .then((body) => {
                        this.exportData = body
                    })
            }, 
            openOperation(name) {
                switch (name) {
                    case 'import': {

                        break;
                    }
                    case 'export': {
                    
                        break;
                    }
                }
            }
        }
      })
})